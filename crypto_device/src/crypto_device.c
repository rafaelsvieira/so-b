#include <linux/init.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/kernel.h>
#include <linux/moduleparam.h>
#include <linux/fs.h>
#include <asm/uaccess.h>
#include <linux/crypto.h>
#include <crypto/hash.h>
#include <crypto/sha.h>
#include <crypto/skcipher.h>
#include <linux/scatterlist.h>


#define DEVICE_NAME "crypto_device"
#define CLASS_NAME  "crypto_cipher_cls"

MODULE_LICENSE("GPL");
MODULE_AUTHOR("\nGuilherme Babugia\nGuilherme Candido\
               \nRafael Vieira\nStephanie Leong\n");
MODULE_DESCRIPTION("Crypto Linux Device Driver");
MODULE_VERSION("0.1");

static char *key = NULL;
module_param(key, charp, 0000);
MODULE_PARM_DESC(key, "Chave necessaria para cifrar e decifrar dados. Deve ser formada por caracteres apenas em hexa.");

static int majorNumber;
static char buffer[1024] = {0};
static short buffer_size;
static struct class*  device_char_class  = NULL;
static struct device* device_char = NULL;
static struct crypto_shash *hash256;
static struct skcipher_def sk;

static int dev_open(struct inode *, struct file *);
static int dev_release(struct inode *, struct file *);
static ssize_t dev_read(struct file *, char *, size_t, loff_t *);
static ssize_t dev_write(struct file *, const char *, size_t, loff_t *);

static struct file_operations fops =
{
	.open = dev_open,
	.read = dev_read,
	.write = dev_write,
	.release = dev_release,
};

struct sdesc {
	struct shash_desc shash;
	char ctx[];
};

struct tcrypt_result {
    struct completion completion;
    int res;
};

struct skcipher_def {
    struct crypto_skcipher *tfm;
    struct skcipher_request *req;
    struct tcrypt_result result;
};

static struct sdesc *init_sdesc(struct crypto_shash *alg)
{
	int size;
	struct sdesc *sdesc;

	size = sizeof(struct shash_desc) + crypto_shash_descsize(alg);
	sdesc = kmalloc(size, GFP_KERNEL);

	if (!sdesc) {
		return ERR_PTR(-ENOMEM);
	}

	sdesc->shash.tfm = alg;
	sdesc->shash.flags = 0x0;

	return sdesc;
}

static char integer_to_hexa(uint8_t value)
{
	uint8_t hexa = value & 0x0F;

	if (hexa <= 9)
		return '0' + hexa;
	else
		return 'A' + hexa - 10;
}

static int crypto_func(uint8_t op, const char *src, char *dst, int len)
{
	char source[16];
	char derived[16];
	int i, rc, blocks, bytes = 0;
	struct scatterlist src_sg, dst_sg;

	if (len == 0 || len % 16) {
		return 0;
	}

	blocks = len / 16;

	sg_init_one(&src_sg, source, 16);
	sg_init_one(&dst_sg, derived, 16);
	skcipher_request_set_crypt(sk.req, &src_sg, &dst_sg, 16, NULL);

	for (i = 0; i < blocks; ++i) {
		(void)memcpy((void *)source, (void *)&src[bytes], 16);

		if (op == 0) {

			rc = crypto_skcipher_encrypt(sk.req);
		} else {
			rc = crypto_skcipher_decrypt(sk.req);
		}

		if (rc == -EINPROGRESS || rc == -EBUSY) {
			wait_for_completion(&sk.result.completion);
			rc = sk.result.res;
		}

		(void)memcpy((void *)&dst[bytes], (void *)derived, 16);
		bytes += 16;
	}

	return bytes;
}

static int crypto_buffer(const char *ubuf, size_t len){
  int i;
  int val;
  int padding;
  int rc = 0;
  const uint8_t zero = 0;

  (void)memcpy((void *)buffer, (void *)&ubuf, len);

  padding = (16 - len % 16) % 16;
  if (padding) {
  	for (i = len; i < len + padding; ++i) {
  		(void)memcpy((void *)&buffer[i], &zero, 1);
  	}
  	len = len + padding;
  }
  rc = crypto_func(0, buffer, buffer, len);
  if (rc > 0) {
  	for (i = len - 1; i >= 0; --i) {
  		val = buffer[i];
  		buffer[i * 2] = integer_to_hexa(val >> 4);
  		buffer[(i * 2) + 1] = integer_to_hexa(val);
  	}
  	buffer_size = len * 2;
    pr_info("Dado cifrado: [%s]", buffer);
  }
  return rc;
}

static void test_skcipher_cb(struct crypto_async_request *req, int rc)
{
    struct tcrypt_result *result = req->data;

    if (rc == -EINPROGRESS)
        return;

    result->res = rc;
    complete(&result->completion);

    pr_info("Encryption finished successfully\n");
}

static int sha256(const unsigned char *data, unsigned int datalen, unsigned char *digest)
{
  int i;
  int ret=0;
  uint8_t val;
  struct sdesc *sdesc;

  sdesc = init_sdesc(hash256);

  if (IS_ERR(sdesc)) {
    pr_info("Erro ao alocar\n");
    return -1;
  }

  ret = crypto_shash_digest(&sdesc->shash, data, datalen, digest);
  kfree(sdesc);
  if (ret == 0) {
    for (i = 0; i < SHA256_DIGEST_SIZE; i++) {
      val = digest[i];
			buffer[i * 2] = integer_to_hexa(val >> 4);
			buffer[(i * 2) + 1] = integer_to_hexa(val);
    }
		buffer_size = SHA1_DIGEST_SIZE * 2;
    pr_info("%s", buffer);
  }

  return ret;
}

static void crypto_hex_to_bin(const unsigned char *message)
{
}

static void decrypto_bin_to_hex(const unsigned char *message)
{
}

static int __init crypto_init(void)
{
  int ret;

	printk(KERN_INFO "crypto_device: Inicializacao modulo crypto.\n");

	if (key == NULL) {
		printk(KERN_ERR "crypto_device: Submeta uma chave AES.\n");
		return -1;
	} else if (strlen(key) != 32) {
		printk(KERN_ERR "crypto_device: Submeta uma chave AES de tamanho 32. Tamanho passado: %zu\n", strlen(key));
		return -1;
	} else {
		printk(KERN_INFO "crypto_device: Chave AES configurada: %s\n", key);
	}

	sk.tfm = crypto_alloc_skcipher("ecb(aes)", 0, 0);
	if (IS_ERR(sk.tfm)) {
		printk(KERN_ERR "crypto: could not allocate skcipher handle\n");
    return PTR_ERR(sk.tfm);
	}

	sk.req = skcipher_request_alloc(sk.tfm, GFP_KERNEL);
  if (!sk.req) {
      printk(KERN_ERR "crypto: could not allocate skcipher request\n");
      ret = -ENOMEM;
      goto fail_request_skcipher;
  }

	skcipher_request_set_callback(sk.req, CRYPTO_TFM_REQ_MAY_BACKLOG, test_skcipher_cb, &sk.result);

	if (crypto_skcipher_setkey(sk.tfm, key, 32)) {
		printk(KERN_ERR "crypto: aes key could not be set\n");
		crypto_free_skcipher(sk.tfm);
		return -EAGAIN;
	}

	majorNumber = register_chrdev(0, DEVICE_NAME, &fops);

	if (majorNumber<0){
		printk(KERN_ALERT "crypto_devie: Falha ao registar major number\n");
		return majorNumber;
	}

	printk(KERN_INFO "crypto_device: Major number registrado como: %d\n", majorNumber);

	device_char_class = class_create(THIS_MODULE, CLASS_NAME);

	if (IS_ERR(device_char_class)) {
		printk(KERN_ALERT "Falha ao registrar device class.\n");
		ret = PTR_ERR(device_char_class);
    goto fail_alloc_class;
	}

	printk(KERN_INFO "crypto_device: device class registrado corretamente.\n");

	device_char = device_create(device_char_class, NULL, MKDEV(majorNumber, 0), NULL, DEVICE_NAME);

	if (IS_ERR(device_char)){
		printk(KERN_ALERT "Falha ao criar o device\n");
		ret = PTR_ERR(device_char);
    goto fail_alloc_device;
	}

  printk(KERN_INFO "crypto_device: device registrado corretamente.\n");

  hash256 = crypto_alloc_shash("sha256", 0, CRYPTO_ALG_ASYNC);
	if (IS_ERR(hash256)) {
		pr_info("crypto_device: Nao foi possivel alocar sha256\n");
		ret = PTR_ERR(hash256);
		goto fail_alloc_hash;
	}

	printk(KERN_INFO "crypto_device: device class criado corretamente.\n");

	return 0;

  fail_alloc_hash:
    crypto_free_shash(hash256);
  fail_alloc_device:
    class_destroy(device_char_class);
  fail_alloc_class:
    unregister_chrdev(majorNumber, DEVICE_NAME);
  fail_request_skcipher:
    skcipher_request_free(sk.req);

  return ret;
}

static void __exit crypto_exit(void)
{
	printk(KERN_INFO "crypto_device: Removendo modulo crypto_device...\n");
	device_destroy(device_char_class, MKDEV(majorNumber, 0));
	class_unregister(device_char_class);
  crypto_free_shash(hash256);
	class_destroy(device_char_class);
	unregister_chrdev(majorNumber, DEVICE_NAME);
	printk(KERN_INFO "crypto_device: Modulo crypto_device removido.\n");
}

static int dev_open(struct inode *inodep, struct file *filep)
{
	printk(KERN_INFO "crypto_device: Device aberto.\n");

	return 0;
}

static ssize_t dev_read(struct file *filep, char *ubuf, size_t len, loff_t *offset)
{
  int ret = 0;
	printk(KERN_INFO "crypto_device: Lendo device.\n");

  decrypto_bin_to_hex(buffer);

  ret = copy_to_user(ubuf, buffer, buffer_size);

  if (ret == 0) {
    printk(KERN_INFO "crypto_devie: Enviando %d caracteres para o usuario\n", buffer_size);
    return (buffer_size = 0);
  } else {
    printk(KERN_INFO "crypto_devie: Falha ao enviar %d caracteres para o usuario\n", ret);
    return -EFAULT;
  }
  return 0;
}

static ssize_t dev_write(struct file *filep, const char *ubuf, size_t len, loff_t *offset)
{
  int rc = 0;
  int start_data = 2;
  unsigned char operation = ubuf[0];
  unsigned char digest[SHA256_DIGEST_SIZE] = {0};

	printk(KERN_INFO "crypto_device: Escrevendo no device.\n");

  crypto_hex_to_bin(ubuf);

  buffer_size = 0;
  switch (operation) {
    case 'h':
      pr_info("Fazendo operacao de SHA256.");
      rc = sha256(&ubuf[start_data], len - start_data, digest);
      break;
    case 'c':
      rc = crypto_buffer(&ubuf[start_data], len - start_data);
      if (rc < 0 ) {
        pr_info("Erro na operacao de cifrar.");
        return rc;
      }
      break;
    default:
      rc = pr_info("Operacao nao implementada!");
      return -1;
      break;
  }
	return len;
}

static int dev_release(struct inode *inodep, struct file *filep)
{
	printk(KERN_INFO "crypto_device: Device encerrado.\n");

	return 0;
}

module_init(crypto_init);
module_exit(crypto_exit);
