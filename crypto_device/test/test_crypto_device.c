#include "stdio.h"
#include "stdlib.h"
#include "errno.h"
#include "fcntl.h"
#include "string.h"
#include "string.h"
#include "unistd.h"

#define BUFFER_LENGTH 1024               ///< The buffer length (crude but fine)

int main(){
    char option = ' ';
    char data[BUFFER_LENGTH];
    char recvData[BUFFER_LENGTH];
    char command = ' ';
    int ret;
    int fd;

    printf("Starting crypto device ...\n");
    fd = open("/dev/crypto_device", O_RDWR);            
    if (fd < 0){
        perror("Failed to open the device...");
        return errno;
    }


    do {
        memset(data, 0, BUFFER_LENGTH);
        memset(recvData, 0, BUFFER_LENGTH);

        printf("Enter an operation [c/d/h data] or 'q' to quit\n");
        fgets(data, BUFFER_LENGTH, stdin);

        option = data[0];

        switch (option) {
                case 'd':
                    printf("Reading from the device...\n");
                    ret = read(fd, recvData, BUFFER_LENGTH);        // Read the response from the LKM
                    if (ret < 0){
                        perror("Failed to read the message from the device.");
                    }
                    printf("The received message is: [%s]\n", recvData);
                    printf("End of the program\n");
                    break;

                case 'h':
                    printf("Writing message to the device");

                    ret = write(fd,data, strlen(data)); // Send the string to the LKM
                    if (ret < 0){
                        perror("Failed to write the message to the device.");
                    }
                    ret = read(fd, recvData, BUFFER_LENGTH);        // Read the response from the LKM
                    if (ret < 0){
                        perror("Failed to read the message from the device.");
                    }
                    printf("The received message is: [%s]\n", recvData);
                    break;
                case 'c':
                    break;
                case 'q':
                    break;
                default:
                    printf("invalid option!");
                    break;

            }
    } while(option != 'q');

    ret = close(fd);
    if (ret < 0) {
        perror("Failed to close the crypto device.");
        return errno;
    }

    return 0;
}
